﻿using UnityEngine;
using System.Collections;

public class QuitOnClick : MonoBehaviour {

    ///<summary>
    ///This function quits the application.
    ///</summary>
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
