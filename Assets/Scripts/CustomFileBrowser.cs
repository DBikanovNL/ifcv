﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Assets.Scripts.Import;
using System.Linq;

namespace Assets.Scripts
{
    public class CustomFileBrowser : MonoBehaviour
    {
        //Skins and textures
        public GUISkin[] skins;
        public Texture2D file, folder, back, drive;

        //Initialize file browser
        FileBrowser fb = new FileBrowser();
        string output = "no file";

        /// <summary>
        /// This function starts automatically. 
        /// This is part of the MonoBehaviour class. 
        /// </summary>
        void Start()
        {
            //Setup file browser style
            fb.guiSkin = skins[0]; //set the starting skin
            fb.fileTexture = file;
            fb.directoryTexture = folder;
            fb.backTexture = back;
            fb.driveTexture = drive;
            //Show the search bar
            fb.showSearch = true;
            //Search recursively (setting recursive search may cause a long delay)
            fb.searchRecursively = true;
        }

        /// <summary>
        /// This function outputs the GUI.  
        /// </summary>
        void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Space(10);
            GUILayout.EndVertical();
            GUILayout.Space(10);
            GUILayout.Label("Selected File: " + output);
            GUILayout.EndHorizontal();
            //Draw and display output
            if (fb.draw())
            { 
                //Return to Main Menu scene when clicking the cancel button
                if (fb.outputFile == null)
                {
                    SceneManager.LoadScene(0);
                }
                //Go to the Viewer scene and save the path of the desired IFC
                else
                {
                    GlobalData.path = fb.outputFile.ToString();
                    SceneManager.LoadScene(2);
                    /*
                    var import = new IfcImport();
                    import.StartImport(output);
                    GlobalData.file = output;
                    
                    var reader = new IfcReader();
                    var elementList = reader.GetAllEntities(output);

                    foreach (IfcEntity entity in elementList)
                    {
                        
                        if (entity.InstanceCnt > 0)
                        {
                            string entityData = "EntityData: \n ";
                            entityData += " ifcEntity: " + entity.ifcEntity + "\n" +
                                            " InstanceCnt: " + entity.InstanceCnt + "\n" +
                                            " Name: " + entity.EntityName + "\n ";
                            //                           Debug.Log(" ifcEntity: " + entity.ifcEntity + "\n" +
                            //                                  " InstanceCnt: " + entity.InstanceCnt + "\n" +
                            //                                  " Name: " + entity.EntityName + "\n ");
                            if (entity.ChildEntity != null)
                            {
                                entityData += "\n ChildEntity: " + entity.ChildEntity.EntityName;

                            }
                            entityData += "\n --- ObjectData ---";
                            var objectList = reader.GetObjectsFromEntity(entity);

                            //entityData += "\n Tags: " + objectList[0].IfcTag;
                            entityData += "\n Count Objects: " + objectList.Count;
                            Debug.Log(entityData);
                        }   
                    }
                   
                    var property = elementList.FirstOrDefault(e => e.EntityName == "IfcBuildingElement");
                    var objectList = reader.GetObjectsFromEntity(property);
                    foreach (IfcObject obj in objectList)
                    {
                        string o = "Objectdata: \n";
                        o += "GameObjectName: " + obj.GameObjectName + "\n";
                        o += "Linenumber: " + obj.LineNumber + "\n";
                        o += "Tag: " + obj.IfcTag + "\n";
                        o += "------------";
                        o += "Valuelist: \n";
                        foreach (IfcArgument arg in obj.ValueList)
                        {
                            o += "Name: " + arg.Name + " Value: " + arg.Value + "\n";
                            
                        }
                        Debug.Log(o);
                    }
                     */
                }
            }
        }
    }
}