﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Assets.Scripts.Import
{
    ///<summary>
    ///This class has functions that read and supply IFC data.
    ///</summary>
    class IfcReader
    {
        private int Model { get; set; }
        private List<IfcObject> _ifcObjectList;
        private List<IfcEntity> _ifcEntityList;

        public IfcReader()
        {
            Model = 0;
            _ifcObjectList = new List<IfcObject>();
            _ifcEntityList = new List<IfcEntity>();
        }

        #region Get entities from IFC file

        ///<summary>
        ///This function will return all the enities in the give IFC file
        ///</summary>
        public List<IfcEntity> GetAllEntities(string pathToIfcFile)
        {
            Debug.Log(pathToIfcFile);
            // Check a model is alread loaded
            if (Model != 0)
            {
                // If so close the model
                x86.sdaiCloseModel(Model);
                Model = 0;
            }

            // Assign the new model to the new file with the standard
            Model = x86.sdaiOpenModelBN(0, pathToIfcFile, @"Assets\Resources\Dependencies\IFC2X3_TC1.exp");

            // Check for the right standard
            IntPtr outputValue;
            x86.GetSPFFHeaderItem(Model, 9, 0, x86.sdaiSTRING, out outputValue);
            
            var s = Marshal.PtrToStringAnsi(outputValue);
            
            if (s.Contains("IFC2") == false)
            {
                x86.sdaiCloseModel(Model);
                Model = 0;
            }
            if (s.Contains("IFC4") == true)
            {
                Model = x86.sdaiOpenModelBN(0, pathToIfcFile, @"Assets\Resources\Dependencies\IFC4.exp");
            }

            // Count the number of entities in the project
            var cnt = x86.engiGetEntityCount(Model);
            for (var i = 0; i < cnt; i++)
            {
                // Add all enities to a list with a reference to the next and the child entity
                var element = x86.engiGetEntityElement(Model, i);

                IntPtr entityNamePtr;
                x86.engiGetEntityName(element, x86.sdaiSTRING, out entityNamePtr);

                var entity = new IfcEntity(element)
                {
                    AttributeCnt = x86.engiGetEntityNoArguments(element),
                    InstanceCnt = x86.sdaiGetMemberCount(x86.sdaiGetEntityExtent(Model, element)),
                    EntityName = Marshal.PtrToStringAnsi(entityNamePtr)
                };

                _ifcEntityList.Add(entity);
            }

            for (var i = 0; i < cnt; i++)
            {
                // Add the child entities
                var ifcParentEntity = x86.engiGetEntityParent(_ifcEntityList[i].ifcEntity);
                if (ifcParentEntity == 0) continue;
                var j = 0;
                while (j < cnt)
                {
                    if (ifcParentEntity == _ifcEntityList[j].ifcEntity)
                    {
                        if (_ifcEntityList[j].ChildEntity != null)
                        {
                            _ifcEntityList[i].NextEntity = _ifcEntityList[j].ChildEntity;
                        }
                        _ifcEntityList[j].ChildEntity = _ifcEntityList[i];
                        _ifcEntityList[i].ParentEntity = _ifcEntityList[j];
                    }
                    j++;
                }
            }

            return _ifcEntityList;
        }

        #endregion

        #region Get objects from IFC file

        ///<summary>
        ///This function will return the objects underneath the given entity
        ///</summary>
        public List<IfcObject> GetObjectsFromEntity(IfcEntity entity)
        {
            // Find all unerlying entities for this entity
            GetAllObjects(entity);

            return _ifcObjectList;
        }

        // Recursive function for finding entities
        private void GetAllObjects(IfcEntity startEntity)
        {
            // Check for new entities
            if (startEntity.ChildEntity != null && startEntity.NextEntity == null)
            {
                var newEntity = startEntity.ChildEntity;
                GetAllObjects(newEntity);
                if (startEntity.InstanceCnt > 0)
                {
                    GetObjectLine(startEntity);
                }
            }
            if (startEntity.ChildEntity != null && startEntity.NextEntity != null)
            {
                var newEntity = startEntity.ChildEntity;
                GetAllObjects(newEntity);
                GetAllObjects(startEntity.NextEntity);
                if (startEntity.InstanceCnt > 0)
                {
                    GetObjectLine(startEntity);
                }
            }
            if (startEntity.ChildEntity == null && startEntity.NextEntity != null)
            {
                GetObjectLine(startEntity);
                GetAllObjects(startEntity.NextEntity);
            }
            if (startEntity.ChildEntity == null && startEntity.NextEntity == null)
            {
                GetObjectLine(startEntity);
            }
        }

        // Get the arguments
        private void GetObjectLine(IfcEntity entity)
        {
            // Get information about the entity
            int ifcColumnInstances = x86.sdaiGetEntityExtentBN(Model,
                System.Text.Encoding.UTF8.GetBytes(entity.EntityName)),
                noIfcColumnInstances = x86.sdaiGetMemberCount(ifcColumnInstances);

            // If ther isn't anything continue
            if (noIfcColumnInstances == 0) return;

            // Loop through the objects of the entity
            for (var i = 0; i < noIfcColumnInstances; i++)
            {
                // Get object information
                int ifcColumnInstance;
                x86.engiGetAggrElement(ifcColumnInstances, i, x86.sdaiINSTANCE,
                    out ifcColumnInstance);

                // Create a new object and add the linenumber to the object
                var obj = new IfcObject(x86.internalGetP21Line(ifcColumnInstance));

                // Get all the arguments from the object
                int ifcEntity = x86.sdaiGetInstanceType(ifcColumnInstance),
                    attributeCnt = x86.engiGetEntityNoArguments(ifcEntity);
                for (var p = 0; p < attributeCnt; p++)
                {
                    // Get the name of the argument
                    IntPtr argumentName;
                    x86.engiGetEntityArgumentName(ifcEntity, p, x86.sdaiUNICODE,
                        out argumentName);
                    // Turn it into a string
                    var entName = Marshal.PtrToStringUni(argumentName);

                    if (entName == null) continue;

                    // Get the argument values
                    IntPtr argumentType;
                    x86.sdaiGetAttrBN(ifcColumnInstance,
                        System.Text.Encoding.UTF8.GetBytes(entName), x86.sdaiUNICODE,
                        out argumentType);

                    // If there aren't any value do not add it to the list
                    if (argumentType == IntPtr.Zero) continue;
                    var entVal = Marshal.PtrToStringUni(argumentType);

                    // Add argument to list
                    obj.ValueList.Add(new IfcArgument() { Name = entName, Value = entVal });
                }
                obj.IfcTag = entity.EntityName;

                // Add object to list
                _ifcObjectList.Add(obj);
            }
        }
        #endregion
    }
}