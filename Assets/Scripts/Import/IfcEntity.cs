﻿namespace Assets.Scripts.Import
{
    internal class IfcEntity
    {
        public int AttributeCnt { get; set; }
        public IfcEntity ChildEntity { get; set; }
        public string EntityName { get; set; }
        public int ifcEntity { get; set; }
        public int InstanceCnt { get; set; }
        public IfcEntity NextEntity { get; set; }
        public IfcEntity ParentEntity { get; set; }

        public IfcEntity(int ifcEnt)
        {
            ifcEntity = ifcEnt;
        }
    }
}