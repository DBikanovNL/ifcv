﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Assets.Scripts.Thread_Ninja;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Import
{
    internal class IfcImport : MonoBehaviour
    {
        private GameObject _buildingObj;
        private GameObject _canvas;
        private GameObject _content;
        private bool _ended;
        private float _loading;
        private GameObject _loadingButton;
        private Text _loadingText;
        private string _path;

        private void Start()
        {
            _canvas = GameObject.Find("Canvas");
            _buildingObj = new GameObject();
            StartImport();
        }

        /// <summary>
        /// This function reads the IFC file and converts it into a .DAE file.
        /// </summary>
        public void StartImport()
        {
            ReadIfc(GlobalData.path);

            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                FileName = Application.dataPath + "/Resources/Dependencies/IfcConvert.exe",
                Arguments =
                "\"--use-world-coords\" \"--sew-shells\" \"--disable-opening-subtractions\" \"--convert-back-units\" \"" +
                GlobalData.path + "\" \"" + Path.GetTempPath() + "\\importBuilding.dae\""
            };

            var process = new Process
            {
                EnableRaisingEvents = true,
                StartInfo = startInfo
            };


            this.StartCoroutineAsync(LoadModel(process));
        }


        /// <summary>
        /// This function start the process to import the ifc file
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private IEnumerator LoadModel(Process process)
        {
            process.Start();
            process.WaitForExit();
            //Executes functions on the thread specific for Unity (RenderModel method)
            yield return Ninja.JumpToUnity;
            RenderModel();
        }

        /// <summary>
        /// After importing the model this function will render the model and make it visible on the screen
        /// </summary>
        private void RenderModel()
        {/*
            var reader = new IfcReader();
            var elementList = reader.GetAllEntities(GlobalData.path);
            var property = elementList.FirstOrDefault(e => e.EntityName == "IfcBuildingElement");
            var objectList = reader.GetObjectsFromEntity(property);
            foreach (IfcObject obj in objectList)
            {
                string o = "Objectdata: \n";
                o += "GameObjectName: " + obj.GameObjectName + "\n";
                o += "Linenumber: " + obj.LineNumber + "\n";
                o += "Tag: " + obj.IfcTag + "\n";
                o += "------------";
                o += "Valuelist: \n";
                foreach (IfcArgument arg in obj.ValueList)
                {
                    o += "Name: " + arg.Name + " Value: " + arg.Value + "\n";

                }
                UnityEngine.Debug.Log(o);
            }*/


            if (File.Exists(Application.dataPath + "/Resources/importBuilding.dae"))
                File.Delete(Application.dataPath + "/Resources/importBuilding.dae");

            if (_buildingObj.activeInHierarchy)
                Destroy(_buildingObj);

            File.Move(Path.GetTempPath() + "/importBuilding.dae", Application.dataPath + "/Resources/importBuilding.dae");

#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
            
            _buildingObj = Instantiate(Resources.Load("importBuilding", typeof(GameObject))) as GameObject;
            _buildingObj.transform.localScale = new Vector3(0.001F, 0.001F, 0.001F);
            _buildingObj.transform.position = new Vector3(7.8F, 0F, 30F);

            foreach (Transform obj in _buildingObj.transform)
            {
                var lineNumber = Convert.ToInt32(obj.name.Substring(0, obj.name.IndexOf('_')));
                var objValues = GlobalData.IfcObjectList.SingleOrDefault(l => l.LineNumber == lineNumber);

                if (objValues == default(IfcObject)) continue;

                var tempGameObject = GameObject.Find(obj.name);
                tempGameObject.AddComponent<BoxCollider>();
                objValues.GameObjectName = tempGameObject.name;
            }
            //_content.AddComponent<ObjectView>();
            
            //_canvas.FindObject("ObjectView").SetActive(true);
            //_canvas.FindObject("ObjectDetails").SetActive(true);
            
        }

        /// <summary>
        /// This function calls the ifc data reader and will add the restults to a static list
        /// </summary>
        /// <param name="fileName"></param>
        private void ReadIfc(string fileName)
        {
            _content = _canvas.FindObject("Content");
/*
            if (_content.transform.childCount > 0)
            {
                foreach (Transform child in _content.transform)
                {
                    Destroy(GameObject.Find(child.name));
                }
                GlobalData.IfcObjectList.Clear();

                Destroy(_content.transform.GetComponent<ObjectView>());
            }
*/
            var reader = new IfcReader();
            var elementList = reader.GetAllEntities(fileName);
            var property = elementList.FirstOrDefault(e => e.EntityName == "IfcBuildingElement");
            GlobalData.IfcObjectList = reader.GetObjectsFromEntity(property);
        }
    }
}