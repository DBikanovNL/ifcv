﻿namespace Assets.Scripts.Import
{
    public class IfcArgument
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return Name + ": " + Value;
        }
    }
}
