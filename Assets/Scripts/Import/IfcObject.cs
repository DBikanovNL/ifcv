﻿using System.Collections.Generic;

namespace Assets.Scripts.Import
{
    public class IfcObject
    {
        public int LineNumber { get; set; }
        public List<IfcArgument> ValueList { get; set; }
        public string GameObjectName { get; set; }
        public string IfcTag { get; set; }

        public IfcObject(int lineNumber)
        {
            LineNumber = lineNumber;
            ValueList = new List<IfcArgument>();
        }

        public override string ToString()
        {
            return "#" + LineNumber;
        }
    }
}
