﻿using Assets.Scripts.Import;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// This class is used to store static values that are used throughout the app. 
/// </summary>
public static class GlobalData
{


    static public string path = "";
    static public List<IfcObject> IfcObjectList;

    public static GameObject FindObject(this GameObject parent, string name)
    {
        var trs = parent.GetComponentsInChildren(typeof(Transform), true);
        return (from t in trs where t.name == name select t.gameObject).FirstOrDefault();
    }
}
